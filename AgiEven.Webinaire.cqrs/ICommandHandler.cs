﻿using System.Threading;
using System.Threading.Tasks;

namespace AgiEven.Webinaire.cqrs
{
    public interface ICommandHandler<in TCommand>
        where TCommand : ICommand
    {
        Task Handle(TCommand command, CancellationToken cancellationToken);
    }
}