﻿using System;

namespace AgiEven.Webinaire.cqrs
{
    public interface ICommand
    {
        public Guid Id { get; }
    }
}
