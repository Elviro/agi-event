using System;
using System.Collections.Generic;

namespace AgiEven.Webinaire.Tests
{
    public class MemoryEventRegistry : IEventRegistry
    {
        public MemoryEventRegistry()
        {
            
        }
        public MemoryEventRegistry(List<EventRegistration> seeding)
        {
            eventRegistrations.AddRange(seeding);
        }
        List<EventRegistration> eventRegistrations = new List<EventRegistration>();

        public IReadOnlyCollection<EventRegistration> Registrations => eventRegistrations.AsReadOnly();

        public void AddRegisterToEnvent(AgiMatricule matricule, int eventId)
        {
            eventRegistrations.Add(new EventRegistration(matricule, eventId));
        }

        public void UnsubscribeToEnvent(AgiMatricule matricule, int eventId)
        {
            var targetEventIndex = eventRegistrations.FindIndex(x => x.EventId == eventId && x.Matricule == matricule);
            if (targetEventIndex == -1)
                throw new ImpossibleToUnsuscribeExeption();

            eventRegistrations.RemoveAt(targetEventIndex);
        }
    }

    public class ImpossibleToUnsuscribeExeption : Exception
    {
    }

    public class EventRegistration
    {
        public readonly AgiMatricule Matricule;
        public readonly int EventId;

        public EventRegistration(AgiMatricule matricule, int eventId)
        {
            Matricule = matricule;
            EventId = eventId;
        }
    }
}