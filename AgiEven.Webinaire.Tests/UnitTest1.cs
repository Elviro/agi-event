using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AgiEven.Webinaire.cqrs;
using NUnit.Framework;

namespace AgiEven.Webinaire.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Should_Agicien_Register_To_Event()
        {
            AgiMatricule matricule = new AgiMatricule("mon matricule");
            int eventId = 12;

            RegisterToEventCommand command = new RegisterToEventCommand(matricule, eventId);

            MemoryEventRegistry registry = new MemoryEventRegistry();

            RegisterToEventCommandHandler handler = new RegisterToEventCommandHandler(registry);
            handler.Handle(command, CancellationToken.None);

            Assert.AreEqual("mon matricule", (string)registry.Registrations.FirstOrDefault().Matricule);
            Assert.AreEqual(12, registry.Registrations.FirstOrDefault().EventId);
        }

        [Test]
        public void Should_Agicien_Unsubscribe_To_Event()
        {
            AgiMatricule matricule = new AgiMatricule("mon matricule");
            int eventId = 12;
            
            MemoryEventRegistry registry = new MemoryEventRegistry(new List<EventRegistration>()
            {
                new EventRegistration(matricule, eventId)
            });

            UnsubscribeToEventCommand command = new UnsubscribeToEventCommand(matricule,eventId);
            UnsubscribeToEventCommandHandler handler = new UnsubscribeToEventCommandHandler(registry);
            handler.Handle(command, CancellationToken.None);


            Assert.AreEqual(0,registry.Registrations.Count);
        }
    }

    public class UnsubscribeToEventCommandHandler : ICommandHandler<UnsubscribeToEventCommand>
    {
        private readonly MemoryEventRegistry _registry;

        public UnsubscribeToEventCommandHandler(MemoryEventRegistry registry)
        {
            _registry = registry;
        }

        public Task Handle(UnsubscribeToEventCommand command, CancellationToken cancellationToken)
        {
            _registry.UnsubscribeToEnvent(command.Matricule, command.EventId);
            return  Task.CompletedTask;
        }
    }

    public class UnsubscribeToEventCommand:ICommand
    {
        public readonly AgiMatricule Matricule;
        public readonly int EventId;
        public Guid Id => Guid.NewGuid();
        public UnsubscribeToEventCommand(AgiMatricule matricule, in int eventId)
        {
            Matricule = matricule;
            EventId = eventId;
        }

    }


    public interface IEventRegistry
    {
        void AddRegisterToEnvent(AgiMatricule matricule, int eventId);
        void UnsubscribeToEnvent(AgiMatricule matricule, int eventId);
    }

    public class RegisterToEventCommandHandler : ICommandHandler<RegisterToEventCommand>
    {
        private readonly IEventRegistry _registry;

        public RegisterToEventCommandHandler(IEventRegistry registry)
        {
            _registry = registry;
        }
        public Task Handle(RegisterToEventCommand command, CancellationToken cancellationToken)
        {
            _registry.AddRegisterToEnvent(command.matricule, command.eventId);
            return Task.CompletedTask;
        }
    }




    public class AgiMatricule
    {
        private readonly string _matricule;

        public AgiMatricule(string matricule)
        {
            _matricule = matricule;
        }

        public static explicit operator string(AgiMatricule matricule) => matricule._matricule;
    }

    public class RegisterToEventCommand : ICommand
    {
        public Guid Id => Guid.NewGuid();
        public readonly AgiMatricule matricule;
        public readonly int eventId ;

        public RegisterToEventCommand(AgiMatricule matricule, int eventId)
        {
            this.matricule = matricule;
            this.eventId = eventId;
        }
    }
}